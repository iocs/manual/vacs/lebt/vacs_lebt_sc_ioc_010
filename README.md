# IOC for LEBT vacuum pumping groups

## Used modules

*   [vac_starting_switchyard](https://gitlab.esss.lu.se/e3/wrappers/vac/e3-vac_starting_switchyard)


## Controlled devices

*   LEBT-010:Vac-VPG-10001
*   LEBT-010:Vac-VPG-20001
