#
# Module: essioc
#
require essioc

#
# Module: vac_starting_switchyard
#
require vac_starting_switchyard


#
# Setting STREAM_PROTOCOL_PATH
#
epicsEnvSet(STREAM_PROTOCOL_PATH, "${vac_starting_switchyard_DB}")


#
# Module: essioc
#
iocshLoad("${essioc_DIR}/common_config.iocsh")

#
# Device: LEBT-010:Vac-VPG-10001
# Module: vac_starting_switchyard
#
iocshLoad("${vac_starting_switchyard_DIR}/vac_starting_switchyard.iocsh", "DEVICENAME = LEBT-010:Vac-VPG-10001, GAUGE_1 = LEBT-010:Vac-VGP-10000, GAUGE_2 = LEBT-010:Vac-VGP-00021, PLC = VacS-ACCV:Vac-PLC-01001")

#
# Device: LEBT-010:Vac-VPG-20001
# Module: vac_starting_switchyard
#
iocshLoad("${vac_starting_switchyard_DIR}/vac_starting_switchyard.iocsh", "DEVICENAME = LEBT-010:Vac-VPG-20001, GAUGE_1 = LEBT-010:Vac-VGP-30000, GAUGE_2 = LEBT-010:Vac-VGP-00081, PLC = VacS-ACCV:Vac-PLC-01001")
